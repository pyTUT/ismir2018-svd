import argparse
import pickle

from sklearn.metrics import confusion_matrix, recall_score, precision_score, f1_score

from .load_data import *

parser = argparse.ArgumentParser()
args = parser.parse_args()


def evaluate(model,  song=None):
    X_test, y_test = load_xy_data(song, FEAT_JAMENDO_DIR + 'test', JAMENDO_LABEL_DIR)
    X_test = X_test.reshape((X_test.shape[0], -1))
    y_pred = model.predict(X_test)
    result = model.score(X_test, y_test)

    # y_pred = median_filter(y_pred, 40, mode='nearest')
    f1 = f1_score(y_test, y_pred, average='binary')
    precision = precision_score(y_test, y_pred, average='binary')
    recall = recall_score(y_test, y_pred, average='binary')

    print("f1", np.mean(f1), "precision", np.mean(precision), "recall", np.mean(recall))

    return y_pred, y_test


if __name__ == '__main__':
    # best model 20180531-1.sav
    test_sets = ['jamendo']


    savefile = './weights/rf.sav'
    loaded_model = pickle.load(open(savefile, 'rb'))

    for test_set in test_sets:
        predicted_values = {}
        y_preds = []
        y_tests = []


        list_of_songs = os.listdir(FEAT_JAMENDO_DIR + 'test')



        for song in list_of_songs:
            print(song)
            y_pred, y_test = evaluate(loaded_model,  song)
            for i in range(len(y_pred)):
                y_preds.append(y_pred[i])
                y_tests.append(y_test[i])

            # pad
            ones = np.ones((INPUT_FRAME_LEN // 2,))
            zeros = np.zeros((INPUT_FRAME_LEN // 2,))
            pred_pad_front = ones if y_pred[0] else zeros
            pred_pad_end = ones if y_pred[-1] else zeros
            test_pad_front = ones if y_test[0] else zeros
            test_pad_end = ones if y_test[-1] else zeros
            y_pred = np.append(pred_pad_front, y_pred)
            y_pred = np.append(y_pred, pred_pad_end)
            y_test = np.append(test_pad_front, y_test)
            y_test = np.append(y_test, test_pad_end)
            print(np.count_nonzero(y_test), np.shape(y_test))

            predicted_values[song] = [y_pred, y_test]
        pickle.dump(predicted_values, open('jamendo.pkl', 'wb'))

        y_preds = np.array(y_preds)
        y_tests = np.array(y_tests)

        # calculate scores 
        acc = (len(y_tests) - np.sum(np.abs(y_preds - y_tests))) / float(len(y_tests))
        f1s = f1_score(y_tests, y_preds, average='binary')
        precisions = precision_score(y_tests, y_preds, average='binary')
        recalls = recall_score(y_tests, y_preds, average='binary')

        tn, fp, fn, tp = confusion_matrix(y_tests, y_preds).ravel()
        acc_confusion = (tp + tn) / (tp + fp + tn + fn).astype(np.float64)
        fp_rate = fp / (fp + tn)
        fn_rate = fn / (fn + tp)

        print("TEST SCORES")
        print('Acc %.4f ' % acc)
        print('Precision %.4f' % precisions)
        print('Recall %.4f' % recalls)
        print('F-1 %.4f' % f1s)
        print('fp rate ', fp_rate, 'fn rate', fn_rate)
