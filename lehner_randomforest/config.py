BASE_DIR = '../'
JAMENDO_DIR = '../jamendo/'  # path to jamendo dataset
JAMENDO_LABEL_DIR = './jamendo/labels/'  # path to jamendo label file
FEAT_JAMENDO_DIR = './jamendo/randomforest_feat_dir/'

# -- Audio processing parameters --#
SR = 16000
FRAME_LEN = 160  # 100ms
HOP_LENGTH = 160  # 100 ms no overlap
MF_WIN_SIZE = 3  # frame length X

INPUT_FRAME_LEN = 10
INPUT_HOP = 1
