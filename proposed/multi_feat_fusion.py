import pickle
import os
from pydub import AudioSegment
from lehner_randomforest.vocal_var import vocal_var as rf_feat_process
from lehner_randomforest.test import evaluate as rf_evaluate
from keras.models import load_model

from schluter_cnn.test import test as cnn_evaluate
from leglaive_lstm.test import test as rnn_evaluate

rf_model = pickle.load(open('./lehner_randomforest/weights/rf.sav', 'rb'))
cnn_model = load_model('./schluter_cnn/weights/cnn.h5')
rnn_model = load_model('./leglaive_lstm/weights/rnn.h5')
song_list = os.listdir('./jamendo/test')
for song in song_list:
    song = song[:-3] + 'npy'
    rf_y_pred, rf_y_test = rf_evaluate(rf_model, song)
    cnn_y_pred_cont, cnn_y_pred, cnn_y_test = cnn_evaluate(cnn_model, song=song)
    rnn_y_pred_cont, rnn_y_pred, rnn_y_test = rnn_evaluate(rnn_model, song)
    print('done')
