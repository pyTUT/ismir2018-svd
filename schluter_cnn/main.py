import argparse

import keras
from keras.callbacks import EarlyStopping, ModelCheckpoint, ReduceLROnPlateau
from keras.optimizer_v2.gradient_descent import SGD
from sklearn.metrics import f1_score, precision_score, recall_score

from load_data import *
from model import *

np.random.seed(0)

parser = argparse.ArgumentParser()
args = parser.parse_args()

# for optimizing threshold value
thresholds = np.arange(1, 100) / 100.0
thresholds = thresholds[:, np.newaxis]

best_sofar = 0.0


class Score_History(keras.callbacks.Callback):
    ''' Callback function for calculating f1, precision, recall scores after every epoch
    '''

    # TODO add init "https://stackoverflow.com/questions/61939790/keras-custom-metrics-self-validation-data-is-none-when-using-data-generators"
    def __init__(self, val_data, batch_size=20):
        super().__init__()
        self.validation_data = val_data
        self.batch_size = batch_size

    def on_epoch_end(self, epoch, logs={}):
        # scores for predicting vocal segments
        y_pred = self.model.predict(self.validation_data[0])
        y_true = self.validation_data[1]

        '''
        y_pred[y_pred >= THRESHOLD] = True
        y_pred[y_pred < THRESHOLD] = False
        '''
        print(y_pred.shape, y_true.shape)
        pred = y_pred[:, 0] >= thresholds
        pred = pred.astype(int)
        true = y_true[:] >= thresholds# TODO change :,0
        true = true.astype(int)
        print(pred.shape, true.shape)

        accs = (((y_true.shape[0] - np.sum(np.abs(pred - true), axis=1))) / float(y_true.shape[0])) * 100.0
        print(accs.shape)
        best = np.argmax(accs)
        print("best threshold, acc", thresholds[best], accs[best])

        accuracy = accs[best]
        f1_ = f1_score(true[best], pred[best], average='binary')
        precision_ = precision_score(true[best], pred[best], average='binary')
        recall_ = recall_score(true[best], pred[best], average='binary')
        '''
        f1_ = f1_score(y_true, y_pred, average='binary')
        precision_ = precision_score(y_true, y_pred, average='binary')
        recall_ = recall_score(y_true, y_pred, average='binary')
        '''

        print("acc: {}, f1 : {}, precision : {}, recall : {}".format(accuracy, f1_, precision_, recall_))
        return


def train():
    model = Schluter_CNN(DROPOUT_RATE)
    model_save_path = './weights/cnn.h5'

    opt = SGD(lr=LEARNING_RATE, momentum=0.9, nesterov=True)
    # opt = Adam(lr=LEARNING_RATE)

    model.compile(loss='binary_crossentropy', optimizer=opt, metrics=['accuracy'])
    print(model.summary())

    checkpoint = ModelCheckpoint(filepath=model_save_path,
                                 monitor='val_acc',
                                 verbose=1,
                                 save_weights_only=False,
                                 save_best_only=True,
                                 mode='auto')
    earlyStopping = EarlyStopping(monitor='val_acc',
                                  patience=EARLY_STOPPING,
                                  verbose=1,
                                  mode='auto')
    reduce_lr = ReduceLROnPlateau(monitor='val_acc',
                                  factor=0.2,
                                  patience=REDUCE_LR,
                                  verbose=1,
                                  min_lr=1e-8)

    x_train, y_train = load_xy_data(None, MEL_JAMENDO_DIR, JAMENDO_LABEL_DIR, 'cnn', 'train')
    x_val, y_val = load_xy_data(None, MEL_JAMENDO_DIR, JAMENDO_LABEL_DIR, "cnn", 'valid')

    print("train, valid data loaded", x_val.shape, y_val.shape)

    # my call back function 
    histories = Score_History((x_val, y_val), BATCH_SIZE)  # TODO add param

    model.fit(x_train, y_train, batch_size=BATCH_SIZE, epochs=NUM_EPOCHS,
              callbacks=[checkpoint, earlyStopping, reduce_lr, histories], shuffle=True, validation_data=(x_val, y_val))

    print("Finished!")


if __name__ == '__main__':
    train()
