import os
from pydub import AudioSegment
data_dir="jamendo"

for item in os.listdir(data_dir+'/filelists'):
    if '.' not in item:
        with open(data_dir+'/filelists/'+item,'r') as item_f:
            cont=item_f.readlines()
            for song_item in cont:
                song_item=song_item.strip('\n')
                if not os.path.exists(data_dir+'/'+item):
                    os.makedirs(data_dir+'/'+item)
                src=data_dir+'/'+"audio/"+song_item
                des=data_dir+'/'+item+'/'+song_item[:-3]+'wav'
                song_audio=AudioSegment.from_file(src,src[-3:])
                song_audio=song_audio.set_channels(1)
                song_audio=song_audio.set_frame_rate(16000)
                song_audio.export(des,'wav')


