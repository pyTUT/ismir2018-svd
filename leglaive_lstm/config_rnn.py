'''
Config for LSTM model 
'''

# jamendo
JAMENDO_DIR = '../jamendo/'  # path to jamendo dataset
JAMENDO_LABEL_DIR = './jamendo/labels/'  # path to jamendo label
MEL_JAMENDO_DIR = './jamendo/ono_leglaive_mel_dir/'

# -- Audio processing parameters --#
SR = 16000
N_FFT1 = 4096
N_HOP1 = 2048
N_FFT2 = 512
N_HOP2 = 256

RNN_INPUT_SIZE = 218  # 7sec/(256/16000)
RNN_OVERLAP = 10
N_MELS = 40
N_MFCC = 40

# -- model parameters --#
BATCH_SIZE = 64
LEARNING_RATE = 0.0001
INPUT_SIZE = 80  # 40 harmonic + 40 percussive
NUM_EPOCHS = 100
THRESHOLD = 0.5
